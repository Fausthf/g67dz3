//
//  ViewController.swift
//  DZ3
//
//  Created by AV on 11/21/18.
//  Copyright © 2018 AV. All rights reserved.
//jhg

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //1 создать строку с своим именем вывести количество символов содержащихся в ней
        let myName = "Anton"
        howManyLetters(name: myName)
        
        //2 создать строку с своим отчеством проверить его на окончание “ич/ на” (в классе написан метод который позволяет проверить на суффикс или префикс, найдите и используйте его)
        let myMiddleName = "Васильевич"
        hisOrHerMiddleName(middleName: myMiddleName)
        
        //3 создать строку где слитно написано Ваши ИмяФамилия
        let enteredName = "AntonNikolayev"
        breakTheString(nameMiddle: enteredName)
        
        //4 вывести строку зеркально Ось -> ьсО не используя reverse (посимвольно)
        let enteredWord = "ToBee"
        reverseTheString(givenString: enteredWord)
        
        //5 добавить запятые в строку как их расставляет калькулятор 1234567 -> 1,234,567 12345 -> 12,345 (не использовать встроенный метод для применения формата)
        let myNumber = "5000000000"
        insertDividers(enteredNumber: myNumber)
        
        // 6 проверить пароль на надежность от 1 до 5
        
        //7 сортировка массива не встроенным методом по возрастанию + удалить дубликаты [9,1,2,5,1,7]
        arrayForHomework()
        
        //8 написать метод который будет переводить строку в транслит - пример print(convertStrToTr4nslite(:”ЯЗЗЬ”)) -> “YAZZ” print(convertStrToTr4nslite:”морДа”) -> “morD4”
        translitTransformer()
        /* 9 сделать выборку из массива строк в которых содержится указанная строка
         [“l4d4”, “sed4n”, “b4kl4zh4n”] se4rch “d4”
         -> [“l4d4”, “sed4n”] - sort() && sort using NSPredic4te + m4nu4l (for loop) */
        
        findSymbolsInArray()
        
        // 10 Set<String> - 4ntim4t [“fuck”, “f4k”] “hello my f4k” “hello my ***”
        
    }
    
    //Functions
    //Assignment 1
    func howManyLetters(name: String) {
        var countLetters = 0
        countLetters = name.count
        print("There are \(countLetters) letters in the name \(name)")
    }
    
    //Assignment 2
    func hisOrHerMiddleName(middleName: String) {
        if middleName.hasSuffix("ич") {
            print("You are male")
        } else if middleName.hasSuffix("на") {
            print("You are female")
        } else {
            print("What are you?")
        }
    }
    
    //Assignment 3
    func breakTheString(nameMiddle: String) {
        var arrayFromString = Array.init(nameMiddle)
        var upperIndex = 0
        for x in 1..<arrayFromString.count { //Finding capitalized letter
            let element = arrayFromString[x]
            let elementUppercased = String(element).uppercased()
            if String(element) == elementUppercased {
                upperIndex = x
            }
        }
        arrayFromString.insert(" ", at: upperIndex) //Inserting the space
        
        let finalString = String(arrayFromString).components(separatedBy: " ")
        let firstName = finalString[0]
        let lastName = finalString[1]
        print(firstName)
        print(lastName)
        print("\(firstName) \(lastName)")
    }
    
    //Assignment 4
    func reverseTheString(givenString: String) {
        var arrayFromString = Array.init(givenString)
        var reversedArray = [Character]()
        for x in 0..<arrayFromString.count {
            let y = (arrayFromString.count - 1) - x
            reversedArray.append(arrayFromString[y])
        }
        print(String(reversedArray))
        
    }
    
    //Assignment 5
    func insertDividers(enteredNumber: String) {
        var characters = Array(enteredNumber)
        for i in stride(from: characters.count, to: 0, by: -3){
            if i != characters.count {
                characters.insert(",", at: i)
            }
        }
        print(String(characters))
    }
    
    //Assignment 7
    func arrayForHomework() {
        var arrayOfNumbers = [Int].init()
        for _ in 0..<20 {
            arrayOfNumbers.append(Int.random(in: 0...10)) //наполнение массива случайными числами в пределах
        }
        print("Generated array of numbers: \(arrayOfNumbers)")
        
        let uniqueNumbers = Set<Int>(arrayOfNumbers)
        arrayOfNumbers = Array(uniqueNumbers)
        
        //Sorting
        for x in 0..<arrayOfNumbers.count {
            for y in x+1..<arrayOfNumbers.count {
                if arrayOfNumbers[x] > arrayOfNumbers[y] {
                    let element2 = arrayOfNumbers.remove(at: y)
                    arrayOfNumbers.insert(element2, at:x)
                }
            }
        }
        print("Sorted array: \(arrayOfNumbers)")
        
    }
    //Assignment 8
    func translitTransformer(){
        let enteredWord = "морда"
        let arrayWord = Array.init(enteredWord)
        var finalStringArr = [Character]()
        let russianAlphabet = Array.init("абвгдезийклмнопрстуфхцэ")
        let translitAlphabet = Array.init("abvgdezijklmnoprstufhce")
        let exeptions = Array.init("жчшщюяь")
        
        //перебор букв в заданом слове
        for x in 0..<arrayWord.count {
            for y in 0..<russianAlphabet.count {
                if arrayWord[x] == russianAlphabet[y] {
                    finalStringArr.append(translitAlphabet[y])
                }
            }
            
        //проверка по словарю исключений
            for e in 0..<exeptions.count {
                if arrayWord[x] == exeptions[e]{
                    switch e {
                    case 0:
                        finalStringArr.append("z")
                        finalStringArr.append("h")
                    case 1:
                        finalStringArr.append("c")
                        finalStringArr.append("h")
                    case 2:
                        finalStringArr.append("s")
                        finalStringArr.append("h")
                    case 3:
                        finalStringArr.append("s")
                        finalStringArr.append("c")
                        finalStringArr.append("h")
                    case 4:
                        finalStringArr.append("j")
                        finalStringArr.append("u")
                    case 5:
                        finalStringArr.append("j")
                        finalStringArr.append("a")
                    case 6:
                        finalStringArr.append("""
'
""")
                    default:
                        print("oops")
                    }
                }
            }
        }
        
        print(String(finalStringArr))
    }
    
    //Assignment 9
    func findSymbolsInArray() {
        let arrayOfContent = ["lada", "sedan", "baklazhan"]
        let searchWord = "la"
        var resultArray = [String]()
        let searchWordSet = Set<Character>(searchWord)
        //NSPredicate
        let predicate = NSPredicate(format: "SELF contains %@", searchWord)
        let search = arrayOfContent.filter {
            predicate.evaluate(with:$0)
        }
        print(search.sorted())
        
        //Manual Search
        for i in 0..<arrayOfContent.count {
            let setFromString = Set<Character>(arrayOfContent[i])
            if setFromString.isSuperset(of: searchWordSet) {
                resultArray.append(arrayOfContent[i])
            }
        }
        print(resultArray.sorted())
    }
    
}
